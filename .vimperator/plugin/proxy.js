var pref = Components.classes["@mozilla.org/preferences-service;1"]
    .getService(Components.interfaces.nsIPrefService);

function toggle_proxy() {
    var socks_proxy_host = pref.getCharPref('network.proxy.socks');
    var socks_proxy_port = pref.getIntPref('network.proxy.socks_port');
    var http_proxy_host = pref.getCharPref('network.proxy.http');
    var http_proxy_port = pref.getIntPref('network.proxy.http_port');
    var proxyType = 1 - pref.getIntPref('network.proxy.type');
    pref.setIntPref('network.proxy.type', proxyType);
    if (proxyType === 0) {
        liberator.echo('no proxy');
    } else {
        var proxies = [];
        if (http_proxy_host && http_proxy_port) {
            proxies.push('http_proxy=' + http_proxy_host + ':' + http_proxy_port);
        }
        if (socks_proxy_host && socks_proxy_port) {
            proxies.push('socks_proxy=' + socks_proxy_host + ':' + socks_proxy_port);
        }
        liberator.echo(proxies.join(', '));
    }
}

commands.addUserCommand(['noproxy'],
    'Switch off proxy',
    function(args) {
        pref.setIntPref('network.proxy.type', 0);
    }, {},
    true
);

commands.addUserCommand(['toggleproxy'],
    'Toggle the proxy',
    function(args) {
        toggle_proxy();
    }, {}, true);
