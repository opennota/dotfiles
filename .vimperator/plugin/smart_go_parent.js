(function() {
    // thanks to http://trac.arantius.com/browser/extension/uppity/content/uppity.js
    let regexp = new RegExp('([a-z]+://)([^/]*)(/.*)');
    let goUpPath = function(path) {
        if (!path) return;
        path = path.replace(/\/$/, '').replace(/^\/+/, '');
        if (path.indexOf('#') > 0) {
            return path.replace(/#.*/, '');
        }
        if (path.indexOf('?') > 0) {
            return path.replace(/\?.*/, '');
        }
        path = path.replace(/\/+$/, '');
        if (path.indexOf('/') > 0) {
            return path.replace(/\/[^\/]*$/, '/');
        }
    }
    let goUpHost = function(host) {
        if (/^[0-9+.:]+$/.test(host)) {
            return host;
        }
        let hostSuffix = '';
        let x = host.lastIndexOf(':');
        if (x > 0) {
            hostSuffix = host.substr(x);
            host = host.substr(0, x);
        }
        hostSuffix = host.substr(host.length - 6) + hostSuffix;
        host = host.substr(0, host.length - 6);
        return host.replace(/[^.]*\./, '') + hostSuffix;
    }
    mappings.addUserMap([modes.NORMAL], ["gu"], 'Smart Go to parent',
        function(count) {
            if (count < 1)
                count = 1;
            let url = buffer.URL;
            let[all, scheme, host, path] = regexp.exec(url);
            path = path.replace(/\/$/, '').replace(/^\/+/, '');
            for (let i = 0; i < count; i++) {
                if (path) {
                    if (path = goUpPath(path)) {
                        url = scheme + host + '/' + path;
                    } else {
                        url = scheme + host + '/';
                    }
                } else {
                    host = goUpHost(host);
                    url = scheme + host + '/';
                }
            }
            if (url == buffer.URL) {
                liberator.beep();
            } else
                liberator.open(url);
        }, {
            count: true
        }
    );
})();
